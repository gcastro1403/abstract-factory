
public class NoSQLFactory implements AbstractFactory<CursosRepository>{

	@Override
	public CursosRepository createRepositoryAlumnos(String type) {
		
		return null;
	}
	
	
	@Override
	public CursosRepository createRepositoryCursos(String type) {
		
		if("SQL".equals(type)) {
					
				return new CursoRelationalRepository();
					
		}
		else if("NOSQL".equals(type)) {
					
				return new CursoNoSQLRepository();
		}
				
		return null;
				
	}
	

}
