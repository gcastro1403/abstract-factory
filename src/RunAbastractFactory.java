
public class RunAbastractFactory {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		AbstractFactory abstractFactoryStudents = ProviderFactory.getFactory("Alumnos");
		AlumnosRepository alumnosSQL = (AlumnosRepository) abstractFactoryStudents.createRepositoryAlumnos("SQL");
		AlumnosRepository alumnosNoSQL = (AlumnosRepository) abstractFactoryStudents.createRepositoryAlumnos("NOSQL");
		
		
		AbstractFactory abstractFactoryCourses = ProviderFactory.getFactory("Cursos");
		CursosRepository coursesNoSQL = (CursosRepository) abstractFactoryCourses.createRepositoryCursos("NOSQL");
		CursosRepository coursesSQL = (CursosRepository) abstractFactoryCourses.createRepositoryCursos("SQL");
		
		System.out.println(alumnosSQL.listaAlumnos());
		System.out.println(alumnosNoSQL.listaAlumnos());
		
		System.out.println(coursesNoSQL.listaCursos());
		System.out.println(coursesSQL.listaCursos());

	}

}
