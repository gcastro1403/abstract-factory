
public class RelationalFactory implements AbstractFactory<AlumnosRepository>{

	@Override
	public AlumnosRepository createRepositoryAlumnos(String type) {
		if("SQL".equals(type)) {
			
			return new AlumnosRelationalRepository();
				
		}
		else if("NOSQL".equals(type)) {
					
			return new AlumnosNoSQLRepository();
		}
				
		return null;
		
	}

	@Override
	public AlumnosRepository createRepositoryCursos(String type) {
		
		return null;
		
	}

}
