import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class AlumnosRelationalRepository implements AlumnosRepository{

	@Override
	public List<String> listaAlumnos() {
		
		List<String> relationalStudents = new ArrayList<>(Arrays.asList("StudentRel1","StudentRel2","StudentRel3","StudentRel4","StudentRel5"));
		return relationalStudents;
	}

}
