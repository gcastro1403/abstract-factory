
public class ProviderFactory {
	
	public static AbstractFactory getFactory(String factoryChosen ) {
		
		if("Alumnos".equals(factoryChosen)) {
			return new RelationalFactory();
		}
		else if("Cursos".equals(factoryChosen)) {
			return new NoSQLFactory();
		}
		
		return null;
	}


}
