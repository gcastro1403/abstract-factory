import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CursoRelationalRepository implements CursosRepository{

	@Override
	public List<String> listaCursos() {
		
		List<String> listRelational = new ArrayList<>(Arrays.asList("Relational1","Relational2","Relational3","Relational4","Relational5"));
		return listRelational;
		
	}

}
